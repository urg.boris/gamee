<?php

namespace Dockata\Exporter;

use Dockata\Template\IDocument;

class JsonExporter extends AExporter
{
    function export(IDocument $document): string
    {
        $jsonExport = '[';
        foreach ($document->getBodyParagraphs()->getBlocks() as $paragraph) {
            $decoratedText = $this->textDecorator->force($paragraph->text);
            $color = $paragraph->color ?? self::DEFAULT_COLOR;
            $jsonExport .= sprintf('{"text":"%s","color":"%s"},', $decoratedText, $color);
        }
        $jsonExport = rtrim($jsonExport, ',');
        $jsonExport .= ']';
        return $jsonExport;
    }
}