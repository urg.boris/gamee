<?php

namespace Dockata\Exporter;

use Dockata\TextDecorator\BaseTextDecorator;
use Dockata\TextDecorator\ITextDecorator;

abstract class AExporter implements IExporter
{
    const DEFAULT_COLOR = 'white';

    /** @var ITextDecorator */
    protected $textDecorator;


    public function __construct(ITextDecorator $textDecorator = null)
    {
        if ($textDecorator === null) {
            $textDecorator = new BaseTextDecorator();
        }
        $this->textDecorator = $textDecorator;
    }
}