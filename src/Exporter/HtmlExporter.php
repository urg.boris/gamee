<?php

namespace Dockata\Exporter;


use Dockata\Template\IDocument;

class HtmlExporter extends AExporter
{

    function export(IDocument $document): string
    {
        $htmlExport = '<html><head></head><body>';

        foreach ($document->getAllParagraphs()->getBlocks() as $paragraph) {
            $color = $paragraph->color ?? self::DEFAULT_COLOR;
            $decoratedText = $this->textDecorator->force($paragraph->text);
            $htmlExport .= sprintf('<p style="color:%s">%s</p>', $color, $decoratedText);
        }
        $htmlExport .= '</body></html>';

        return $htmlExport;
    }
}