<?php

namespace Dockata\Exporter;

use Dockata\DataContainer\Config;
use Dockata\Template\IDocument;

interface IExporter
{
    function export(IDocument $document): string;
}