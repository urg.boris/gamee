<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Dockata\DataContainer\ParagraphBlock;
use Dockata\Exporter\HtmlExporter;
use Dockata\Exporter\JsonExporter;
use Dockata\Template\MyDocBasic;
use Dockata\Template\MyDocWithAlternativeFooter;
use Dockata\Template\MyDocWithoutHeaderAndFooter;
use Dockata\TextDecorator\BaseTextDecorator;
use Dockata\TextDecorator\Positive;
use Dockata\TextDecorator\Smile;
use Dockata\TextDecorator\Jesus;


$baseData = [
    // thanks http://officeipsum.com
    [
        'text' => 'Bad commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.',
        'color' => 'blue',
    ],
    [
        'text' => 'Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.',
        'color' => 'red',
    ],
    [
        'text' => 'Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.',
        'color' => 'yellow',
    ],
];

try {
    $doc = MyDocBasic::fromTextArray($baseData);
} catch (Exception $e) {
    echo "wrong input ".$e->getMessage();
    die;
}
$doc2 = new MyDocWithoutHeaderAndFooter($doc->getBodyParagraphs());
$doc3 = new MyDocWithAlternativeFooter($doc->getBodyParagraphs());

$baseTextDecorator = new BaseTextDecorator();
$positiveDecorator = new Positive($baseTextDecorator);
$smileDecorator = new Smile($baseTextDecorator);
$happinessDecorator = new Smile($positiveDecorator);
$jesusDecorator = new Jesus($baseTextDecorator);

$htmlExporter = new HtmlExporter($happinessDecorator);
$jsonExporter = new JsonExporter($jesusDecorator);

echo $htmlExporter->export($doc);

echo '


';

echo $jsonExporter->export($doc2);

echo '


';

echo $jsonExporter->export($doc3);