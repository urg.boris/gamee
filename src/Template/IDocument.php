<?php

namespace Dockata\Template;

use Dockata\DataContainer\ParagraphBlock;

interface IDocument
{
    public function getAllParagraphs(): ParagraphBlock;

    public function getBodyParagraphs(): ParagraphBlock;

}