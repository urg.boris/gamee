<?php

namespace Dockata\Template;

use Dockata\DataContainer\ParagraphBlock;

class MyDocWithoutHeaderAndFooter implements IDocument
{
    /**
     * @var ParagraphBlock
     */
    protected $bodyParagraphs;

    /**
     * @param ParagraphBlock $bodyParagraphs
     */
    public function __construct(ParagraphBlock $bodyParagraphs)
    {
        $this->bodyParagraphs = $bodyParagraphs;
    }

    /**
     * @param array<int, array<string>> $textArray
     * @return MyDocWithoutHeaderAndFooter
     * @throws \Exception
     */
    public static function fromTextArray(array $textArray): IDocument
    {
        $paragraphBlock = ParagraphBlock::fromTextArray($textArray);
        return new MyDocWithoutHeaderAndFooter($paragraphBlock);
    }


    public function getAllParagraphs(): ParagraphBlock
    {
        return $this->bodyParagraphs;
    }

    public function getBodyParagraphs(): ParagraphBlock
    {
        return $this->bodyParagraphs;
    }
}