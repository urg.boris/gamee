<?php

namespace Dockata\Template;

use Dockata\DataContainer\Paragraph;
use Dockata\DataContainer\ParagraphBlock;

class MyDocBasic implements IDocument
{
    /**
     * @var ParagraphBlock
     */
    protected $bodyParagraphs;

    /**
     * @var Paragraph
     */
    protected $headedParagraph;

    /**
     * @var Paragraph
     */
    protected $footerParagraph;

    public function __construct(ParagraphBlock $paragraphBlock)
    {
        $this->bodyParagraphs = $paragraphBlock;
        $this->addHeader();
        $this->addFooter();
    }

    /**
     * @param array<int, array<string>> $textArray
     * @return MyDocBasic
     * @throws \Exception
     */
    public static function fromTextArray(array $textArray): IDocument
    {
        $paragraphBlock = ParagraphBlock::fromTextArray($textArray);
        return new MyDocBasic($paragraphBlock);
    }

    public function getAllParagraphs(): ParagraphBlock
    {
        return $this->bodyParagraphs
            ->withPrependBlock($this->headedParagraph)
            ->withAppendBlock($this->footerParagraph);
    }

    public function getBodyParagraphs(): ParagraphBlock
    {
        return $this->bodyParagraphs;
    }

    protected function addHeader(): void
    {
        $this->headedParagraph = new Paragraph(
            [
                'text' => ' BIG HEADER ',
            ]
        );
    }

    protected function addFooter(): void
    {
        $this->footerParagraph = new Paragraph(
            [
                'text' => ' small footer ',
            ]
        );
    }
}