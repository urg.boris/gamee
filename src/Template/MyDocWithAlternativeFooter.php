<?php

namespace Dockata\Template;

use Dockata\DataContainer\Paragraph;
use Dockata\DataContainer\ParagraphBlock;

class MyDocWithAlternativeFooter extends MyDocBasic
{

    public function __construct(ParagraphBlock $paragraphBlock)
    {
        parent::__construct($paragraphBlock);
    }

    /**
     * @param array<int, array<string>> $textArray
     * @return MyDocWithAlternativeFooter
     * @throws \Exception
     */
    public static function fromTextArray(array $textArray): IDocument
    {
        $paragraphBlock = ParagraphBlock::fromTextArray($textArray);
        return new MyDocWithAlternativeFooter($paragraphBlock);
    }

    protected function addFooter(): void
    {
        $this->footerParagraph = new Paragraph(
            [
                'text' => ' alternative footer ',
            ]
        );
    }

}