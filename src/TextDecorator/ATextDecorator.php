<?php

namespace Dockata\TextDecorator;

abstract class ATextDecorator implements ITextDecorator
{
    /** @var ITextDecorator */
    protected $moodDecorator;

    /**
     * @param ITextDecorator $moodDecorator
     */
    public function __construct(ITextDecorator $moodDecorator)
    {
        $this->moodDecorator = $moodDecorator;
    }

    public function force(string $text): string
    {
        return $this->moodDecorator->force($text);
    }
}