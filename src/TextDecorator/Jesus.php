<?php

namespace Dockata\TextDecorator;

class Jesus extends ATextDecorator
{
    /** @var string */
    private $jesus = "Jesus said: ";

    public function force(string $text): string
    {
        return $this->jesus . $this->moodDecorator->force($text);
    }
}