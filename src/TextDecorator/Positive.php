<?php

namespace Dockata\TextDecorator;

class Positive extends ATextDecorator
{

    /** @var string[] */
    private $wordReplacement = [
        "bad" => "good",
        "problem" => "opportunity"
    ];

    /** @var string[] */
    private $replacements;

    public function __construct(ITextDecorator $moodDecorator)
    {
        parent::__construct($moodDecorator);
        $this->replacements = [];
        foreach ($this->wordReplacement as $originalWord => $replacementWord) {
            $this->replacements[strtolower($originalWord)] = strtolower($replacementWord);
            $this->replacements[strtoupper($originalWord)] = strtoupper($replacementWord);
            $this->replacements[ucfirst(strtolower($originalWord))] = ucfirst(strtolower($replacementWord));
        }
    }


    public function force(string $text): string
    {
        $alteredText = $this->moodDecorator->force($text);
        foreach ($this->replacements as $origWord => $replacement) {
            $alteredText = str_replace($origWord, $replacement, $alteredText);
        }
        return $alteredText;
    }
}