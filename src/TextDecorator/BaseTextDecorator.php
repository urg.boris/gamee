<?php

namespace Dockata\TextDecorator;

class BaseTextDecorator implements ITextDecorator
{
    public function force(string $text): string
    {
        return $text;
    }
}