<?php

namespace Dockata\TextDecorator;

interface ITextDecorator
{
    public function force(string $text): string;
}