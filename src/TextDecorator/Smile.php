<?php

namespace Dockata\TextDecorator;

class Smile extends ATextDecorator
{
    /** @var string */
    private $smile = " :)";

    public function force(string $text): string
    {
        return $this->moodDecorator->force($text) . $this->smile;
    }
}