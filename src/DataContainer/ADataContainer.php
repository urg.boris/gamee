<?php

namespace Dockata\DataContainer;


abstract class ADataContainer
{
    /** @param string[] $paramsInArray
     * @throws \Exception
     */
    function __construct(array $paramsInArray)
    {
        $this->fillPropertiesFromArray($paramsInArray);
    }


    /** @param string[] $paramsInArray
     * @throws \Exception
     */
    protected function fillPropertiesFromArray(array $paramsInArray): void
    {
        $classAttribs = array_keys(get_object_vars($this));

        foreach ($paramsInArray as $paramName => $paramValue) {
            $classHasAttribute = in_array($paramName, $classAttribs);

            if (!$classHasAttribute) {
                throw new \Exception("no such attribute here! class missing attrib '$paramName'");
            }

            $this->{$paramName} = $paramValue;
        }
    }
}