<?php

namespace Dockata\DataContainer;

class ParagraphBlock
{
    /** @var Paragraph[] */
    protected $blocks;

    private function __construct()
    {

    }


    /**
     * @return Paragraph[]
     */
    public function getBlocks(): array
    {
        return $this->blocks;
    }

    /**
     * @return array<int, array<string>>
     */
    public function toStringArray(): array
    {
        $array = [];
        foreach ($this->blocks as $block) {
            $array[] = $block->toStringArray();
        }
        return $array;
    }

    /**
     * @param array<int, array<string>> $textArray
     * @return ParagraphBlock
     * @throws \Exception
     */
    public static function fromTextArray(array $textArray): ParagraphBlock
    {
        $paragraphBlock = new self();
        $paragraphBlock->blocks = [];
        foreach ($textArray as $block) {
            $paragraphBlock->blocks[] = new Paragraph($block);
        }
        return $paragraphBlock;
    }

    /**
     * @param Paragraph[] $paragraphs
     * @return ParagraphBlock
     */
    protected static function fromParagraphsArray(array $paragraphs): ParagraphBlock
    {
        $paragraphBlock = new self();
        $paragraphBlock->blocks = [];
        foreach ($paragraphs as $block) {
            $paragraphBlock->blocks[] = $block;
        }
        return $paragraphBlock;
    }

    /**
     * @param Paragraph $paragraph
     * @return ParagraphBlock
     */
    public function withPrependBlock(Paragraph $paragraph): ParagraphBlock
    {
        $blocks = $this->blocks;
        array_unshift($blocks, $paragraph);
        return self::fromParagraphsArray($blocks);
    }

    /**
     * @param Paragraph $paragraph
     * @return ParagraphBlock
     */
    public function withAppendBlock(Paragraph $paragraph): ParagraphBlock
    {
        $blocks = $this->blocks;
        $blocks[] = $paragraph;
        return self::fromParagraphsArray($blocks);

    }


}