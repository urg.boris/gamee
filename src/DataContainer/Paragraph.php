<?php

namespace Dockata\DataContainer;


class Paragraph extends ADataContainer
{

    const TEXT_FIELD_NAME = 'text';

    /** @var string $text */
    public $text;

    /** @var string|null $color */
    public $color;

    /** @var string[] */
    protected $obligatoryFields;

    public function __construct(array $paramsInArray)
    {
        parent::__construct($paramsInArray);
        $this->obligatoryFields = [self::TEXT_FIELD_NAME];
        foreach ($this->obligatoryFields as $field) {
            if (!in_array($field, array_keys($paramsInArray))) {
                throw new \Exception('missing field '.$field);
            }
        }

    }


    /**
     * @return string[]
     */
    public function toStringArray(): array
    {
        $out = [];
        $out['text'] = $this->text;
        if ($this->color !== null) {
            $out['color'] = $this->color;
        }
        return $out;
    }
}