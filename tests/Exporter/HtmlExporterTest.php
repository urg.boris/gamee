<?php

namespace Dockata\Tests\Exporter;

use Dockata\Exporter\HtmlExporter;
use Dockata\Template\MyDocBasic;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

final class HtmlExporterTest extends TestCase
{
    public function dataProviderForExportWithBasicDoc(): array
    {
        return [
            [
                [],
                '<html><head></head><body><p style="color:white"> BIG HEADER </p><p style="color:white"> small footer </p></body></html>',
            ],
            [
                [
                    [
                        'text' => 'Bad commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.',
                        'color' => 'blue',
                    ],
                    [
                        'text' => 'Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.',
                        'color' => 'red',
                    ],
                    [
                        'text' => 'Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.',
                        'color' => 'yellow',
                    ],
                ],
                '<html><head></head><body><p style="color:white"> BIG HEADER </p><p style="color:blue">Bad commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.</p><p style="color:red">Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.</p><p style="color:yellow">Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.</p><p style="color:white"> small footer </p></body></html>',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForExportWithBasicDoc
     */
    public function testExportWithBasicDoc(array $data, string $expected): void
    {
        $doc = MyDocBasic::fromTextArray($data);
        $exporter = new HtmlExporter();
        $exportedHtml = $exporter->export($doc);
        Assert::equal($expected, $exportedHtml);
    }
}

(new HtmlExporterTest())->run();