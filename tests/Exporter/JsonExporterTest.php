<?php

namespace Dockata\Tests\Exporter;

use Dockata\Exporter\JsonExporter;
use Dockata\Template\MyDocWithoutHeaderAndFooter;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

final class JsonExporterTest extends TestCase
{
    public function dataProviderTestData(): array
    {
        return
            [
                [
                    [],
                    "[]"
                ],
                [
                    [
                        // thanks http://officeipsum.com
                        [
                            'text' => 'Bad commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.',
                            'color' => 'blue',
                        ],
                        [
                            'text' => 'Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.',
                            'color' => 'red',
                        ],
                        [
                            'text' => 'Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.',
                            'color' => 'yellow',
                        ],
                    ],
                    '[{"text":"Bad commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.","color":"blue"},{"text":"Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.","color":"red"},{"text":"Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.","color":"yellow"}]'
                ],
                [
                    [
                        // thanks http://officeipsum.com
                        [
                            'text' => 'commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.',
                            'color' => 'blue',
                        ],
                        [
                            'text' => 'Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.',
                            'color' => 'red',
                        ],
                        [
                            'text' => 'Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.',
                        ],
                    ],
                    '[{"text":"commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.","color":"blue"},{"text":"Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.","color":"red"},{"text":"Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.","color":"white"}]'
                ]
            ];
    }

    /**
     * @dataProvider dataProviderTestData
     */
    public function testExport1(array $inputData, string $expectedText): void
    {
        $doc = MyDocWithoutHeaderAndFooter::fromTextArray($inputData);
        $exporter = new JsonExporter();
        $exportedText = $exporter->export($doc);
        Assert::equal($expectedText, $exportedText);
    }
}

(new JsonExporterTest())->run();