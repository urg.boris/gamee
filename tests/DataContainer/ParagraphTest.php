<?php

declare(strict_types=1);

namespace Dockata\Tests\DataContainer;

use Dockata\DataContainer\Paragraph;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class ParagraphTest extends TestCase
{
    /**
     * @dataProvider dpParagraphArgs
     */
    public function testParagraph(array $inputData): void
    {
        $p = new Paragraph($inputData);
        Assert::equal($inputData, $p->toStringArray());
    }

    public function dpParagraphArgs(): array
    {
        return [
            [
                [
                    'text' => 'paragraph',
                ]
            ],
            [
                [
                    'text' => 'paragraph',
                    'color' => 'red',
                ]
            ]
        ];
    }


    public function testExceptions(): void
    {
        $c = [
            'text' => 'paragraph',
            'color' => 'red',
            'notAProperty' => 'notAValue'
        ];

        Assert::exception(
            function () use ($c) {
                new Paragraph($c);
            },
            \Exception::class,
            "no such attribute here! class missing attrib 'notAProperty'");

        $c = [
            'color' => 'red',
        ];

        Assert::exception(
            function () use ($c) {
                new Paragraph($c);
            },
            \Exception::class,
            'missing field text'
        );
    }
}

(new ParagraphTest())->run();
