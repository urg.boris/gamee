<?php

declare(strict_types=1);

namespace Dockata\Tests\DataContainer;

use Dockata\DataContainer\Paragraph;
use Dockata\DataContainer\ParagraphBlock;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class ParagraphBlockTest extends TestCase
{
    public function testCreation(): void
    {
        $config = [
            [
                'text' => 'Bad commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.',
                'color' => 'blue',
            ],
            [
                'text' => 'Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.',
                'color' => 'red',
            ],
            [
                'text' => 'Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.',
                'color' => 'yellow',
            ]
        ];
        $paragraphBlock = ParagraphBlock::fromTextArray($config);

        Assert::count(3, $paragraphBlock->getBlocks());
        Assert::equal($config, $paragraphBlock->toStringArray());

        $prependConfig = [
            'text' => 'prepend'
        ];

        $prependParagraph = new Paragraph($prependConfig);
        $preParagraphBlock = $paragraphBlock->withPrependBlock($prependParagraph);
        Assert::count(4, $preParagraphBlock->toStringArray());

        $appendConfig = [
            'text' => 'append',
            'color' => 'white'
        ];

        $appendParagraph = new Paragraph($appendConfig);
        $appendParagraphBlock = $paragraphBlock->withAppendBlock($appendParagraph);
        Assert::count(4, $appendParagraphBlock->toStringArray());

        $combinedBlocks = $paragraphBlock
            ->withAppendBlock($appendParagraph)
            ->withPrependBlock($prependParagraph)
            ->toStringArray();

        Assert::count(5, $combinedBlocks);
        $expectedCombined = [
            [
                'text' => 'prepend'
            ],
            [
                'text' => 'Bad commitment to the cause it just needs more cowbell nor pipeline digitalize. Low-hanging fruit programmatically we need distributors to evangelize the new line to local markets, so message the initiative win-win-win nor organic growth.',
                'color' => 'blue',
            ],
            [
                'text' => 'Parallel path quarterly sales are at an all-time low, that\'s a huge problem. Goalposts on this journey. Come up with something buzzworthy put in in a deck for our standup today nor back of the net. Not enough bandwidth we are running out of runway.',
                'color' => 'red',
            ],
            [
                'text' => 'Reach out we need to dialog around your choice of work attire customer centric, yet synergestic actionables cannibalize. Going forward nail jelly to the hothouse wall, yet it\'s a simple lift and shift job, quick win.',
                'color' => 'yellow',
            ],
            [
                'text' => 'append',
                'color' => 'white'
            ]
        ];

        Assert::equal($expectedCombined, $combinedBlocks);


    }
}

(new ParagraphBlockTest())->run();