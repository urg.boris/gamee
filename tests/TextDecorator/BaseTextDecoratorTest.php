<?php

declare(strict_types=1);

namespace Dockata\Tests\TextDecorator;

use Dockata\TextDecorator\BaseTextDecorator;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class BaseTextDecoratorTest extends TestCase
{

    public function testWords(): void
    {
        $testData = 'something';
        $baseDecorator = new BaseTextDecorator();
        Assert::equal($testData, $baseDecorator->force($testData));
    }

}

(new BaseTextDecoratorTest())->run();
