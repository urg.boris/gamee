<?php

declare(strict_types=1);

namespace Dockata\Tests\TextDecorator;

use Dockata\TextDecorator\BaseTextDecorator;
use Dockata\TextDecorator\Jesus;
use Dockata\TextDecorator\Positive;
use Dockata\TextDecorator\Smile;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class CombinedTest extends TestCase
{

    /**
     * @dataProvider dataProviderInputs
     */
    public function testOutput(string $input, string $expected): void
    {
        $combinedDecorator = new Jesus(new Positive(new Smile(new BaseTextDecorator())));
        Assert::equal($combinedDecorator->force($input), $expected);
    }

    public function dataProviderInputs(): array
    {
        return [
            [
                'The situation is really bad. We have a huge problem in here.',
                'Jesus said: The situation is really good. We have a huge opportunity in here. :)'
            ]
        ];
    }

}

(new CombinedTest())->run();
