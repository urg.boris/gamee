<?php

declare(strict_types=1);

namespace Dockata\Tests\TextDecorator;

use Dockata\TextDecorator\BaseTextDecorator;
use Dockata\TextDecorator\Positive;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class PositiveTest extends TestCase
{

    /**
     * @dataProvider dataProviderWords
     */
    public function testWords(string $input, string $expected): void
    {
        $baseDecorator = new BaseTextDecorator();
        $positiveDecorator = new Positive($baseDecorator);
        Assert::equal($positiveDecorator->force($input), $expected);
    }

    public function dataProviderWords(): array
    {
        return [
            [
                'The situation is really bad. We have a huge problem in here.',
                'The situation is really good. We have a huge opportunity in here.'
            ],
            [
                'Reach out we need to dialog around your choice of work attire customer centric',
                'Reach out we need to dialog around your choice of work attire customer centric'
            ]
        ];
    }

}

(new PositiveTest())->run();
