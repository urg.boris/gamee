<?php

declare(strict_types=1);

namespace Dockata\Tests\TextDecorator;

use Dockata\TextDecorator\BaseTextDecorator;
use Dockata\TextDecorator\Smile;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class SmileTest extends TestCase
{

    /**
     * @dataProvider dataProviderInputs
     */
    public function testSmiles(string $input, string $expected): void
    {
        $baseDecorator = new BaseTextDecorator();
        $positiveDecorator = new Smile($baseDecorator);
        Assert::equal($positiveDecorator->force($input), $expected);
    }

    public function dataProviderInputs(): array
    {
        return [
            [
                'The situation is really bad. We have a huge problem in here.',
                'The situation is really bad. We have a huge problem in here. :)'
            ]
        ];
    }

}

(new SmileTest())->run();
