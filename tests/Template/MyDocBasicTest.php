<?php

declare(strict_types=1);

namespace Dockata\Tests\Template;

use Dockata\DataContainer\ParagraphBlock;
use Dockata\Template\MyDocBasic;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class MyDocBasicTest extends TestCase
{
    public function testMyDoc(): void
    {
        $inputData =
            [
                [
                    'text' => 'paragraph',
                ],
                [
                    'text' => 'paragraph',
                    'color' => 'red',
                ],
            ];
        $doc = MyDocBasic::fromTextArray($inputData);
        Assert::count(4, $doc->getAllParagraphs()->getBlocks());
        Assert::equal($inputData, $doc->getBodyParagraphs()->toStringArray());


        $expectedResult = [
            [
                'text' => ' BIG HEADER ',
            ],
            $inputData[0],
            $inputData[1],
            [
                'text' => ' small footer ',
            ],
        ];


        Assert::equal($expectedResult, $doc->getAllParagraphs()->toStringArray());
    }
}

(new MyDocBasicTest())->run();
