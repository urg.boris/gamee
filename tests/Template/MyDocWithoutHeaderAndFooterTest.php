<?php

declare(strict_types=1);

namespace Dockata\Tests\Template;

use Dockata\DataContainer\ParagraphBlock;
use Dockata\Template\MyDocWithoutHeaderAndFooter;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

class MyDocWithoutHeaderAndFooterTest extends TestCase
{
    public function testDocWithoutHeaderAndFooterTest(): void
    {
        $inputData =
            [
                [
                    'text' => 'paragraph',
                ],
                [
                    'text' => 'paragraph',
                    'color' => 'red',
                ],
            ];
        $doc = MyDocWithoutHeaderAndFooter::fromTextArray($inputData);
        Assert::count(2, $doc->getAllParagraphs()->getBlocks());
        Assert::equal($inputData, $doc->getBodyParagraphs()->toStringArray());

        $expectedResult = [
            $inputData[0],
            $inputData[1],
        ];

        Assert::equal($expectedResult, $doc->getAllParagraphs()->toStringArray());
    }
}

(new MyDocWithoutHeaderAndFooterTest())->run();
